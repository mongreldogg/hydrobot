<?php

use Core\Event;
use Core\Template;
use Core\Request;
use Core\Response;

use Bundle\FaqQnA;
use Bundle\FaqCategory;

Event::add('web::faq', function(){
	
	@$action = Request::Request()['act'];
	$response = Template::Get('web/main');
	$content = null;
	FaqQnA::InitDefaultConnection();
	switch($action){
		case 'edit_category':
			$content = Event::raise('web::faq::edit_category');
			break;
		case 'add_category':
			$content = Event::raise('web::faq::add_category');
			break;
		case 'delete_category':
			$content = Event::raise('web::faq::delete_category');
			break;
		case 'question_list':
			$content = Event::raise('web::faq::question_list');
			break;
		case 'add_question':
			$content = Event::raise('web::faq::add_question');
			break;
		case 'edit_question':
			$content = Event::raise('web::faq::edit_question');
			break;
		case 'delete_question':
			$content = Event::raise('web::faq::delete_question');
			break;
		default:
			$content = Event::raise('web::faq::cat_list');
			break;
	}
	$response = Template::Render($response, [
		'content' => $content
	]);
	return $response;
	
});

Event::add('web::faq::delete_question', function(){
	
	$qId = (int)Request::Request()['id'];
	FaqQnA::Delete(['id' => $qId]);
	Response::JSON(['status' => 'OK', 'delete_id' => $qId]);
	exit;
	
});

Event::add('web::faq::add_question', function(){
	
	$question = Request::Request()['faq_question'];
	$answer = Request::Request()['faq_answer'];
	$category = (int)Request::Request()['category_id'];
	
	$question = new FaqQnA(['category_id' => $category, 'faq_question' => $question, 'faq_answer' => $answer]);
	$question->Save();
	Response::JSON(['status' => 'OK', 'item' => $question->Serialize()]);
	exit;
	
});

Event::add('web::faq::edit_question', function(){
	
	$questionId = (int)Request::Request()['id'];
	if($questionId == null) {
		Response::JSON([
			'status' => 'failed',
			'message' => 'no_question'
		]);
		exit;
	}
	$question = Request::Request()['faq_question'];
	$answer = Request::Request()['faq_answer'];
	$q = FaqQnA::Select(['id' => $questionId], 1);
	if($q instanceof FaqQnA) {
		$q->setQuestion($question);
		$q->setAnswer($answer);
		$q->Save();
		Response::JSON(['status' => 'OK', 'item' => $q->Serialize()]);
		exit;
	} else {
		Response::JSON([
			'status' => 'failed',
			'message' => 'no_question'
		]);
		exit;
	}
	
});

Event::add('web::faq::delete_category', function(){
	
	$categoryId = (int)Request::Request()['id'];
	FaqCategory::Delete(['id' => $categoryId]);
	Response::JSON(['status' => 'OK', 'delete_id' => $categoryId]);
	exit;
	
});

Event::add('web::faq::add_category', function(){
	
	$categoryName = Request::Request()['category'];
	$category = new FaqCategory(['category' => $categoryName]);
	$category->Save();
	Response::JSON(['status' => 'OK', 'item' => $category->Serialize()]);
	exit;
	
});

Event::add('web::faq::edit_category', function(){
	
	$categoryId = (int)Request::Request()['id'];
	if($categoryId == null) {
		Response::JSON([
			'status' => 'failed',
			'message' => 'no_category'
		]);
		exit;
	}
	$categoryName = Request::Request()['category'];
	$category = FaqCategory::Select(['id' => $categoryId], 1);
	if($category instanceof FaqCategory) {
		$category->setCategoryName($categoryName);
		$category->Save();
		Response::JSON(['status' => 'OK', 'item' => $category->Serialize()]);
		exit;
	} else {
		Response::JSON([
			'status' => 'failed',
			'message' => 'no_category'
		]);
		exit;
	}
	
});

Event::add('web::faq::cat_list', function(){
	
	$template = Template::Get('web/cat_list');
	$list_template = Template::Tag('cat', $template, '{cats}');
	$cats = FaqCategory::Select([]);
	$counter = 1;
	$list = '';
	if(is_array($cats))
	foreach($cats as $cat){
		if($cat instanceof FaqCategory) $list .= Template::Replace(
			array_merge($cat->Serialize(), ['counter' => $counter++]),
			$list_template);
	}
	$result = Template::Render($template, ['cats' => $list]);
	return $result;
	
});

Event::add('web::faq::question_list', function(){
	
	@$category = (int)Request::Request()['category'];
	if($category == null) Response::Redirect('admin');
	$template = Template::Get('web/question_list');
	$list_template = Template::Tag('question', $template, '{questions}');
	$questions = FaqQnA::Select(['category_id' => $category]);
	Template::SetGlobal('categoryId', $category);
	$category = FaqCategory::Select(['id' => $category], 1);
	if($category instanceof FaqCategory) $category = $category->getCategoryName();
	$counter = 1;
	$list = '';
	if(is_array($questions))
		foreach($questions as $q){
			if($q instanceof FaqQnA) {
				$q = $q->Serialize();
				$q['counter'] = $counter++;
				$q['short_q'] =
					strlen($q['faq_question']) > 48 ? substr($q['faq_question'], 0, 48)."..." : $q['faq_question'];
				$q['short_a'] =
					strlen($q['faq_answer']) > 48 ? substr($q['faq_answer'], 0, 48)."..." : $q['faq_answer'];
				$list .= Template::Replace($q, $list_template);
			}
		}
	$result = Template::Render($template, ['questions' => $list, 'catName' => $category]);
	return $result;
	
});
