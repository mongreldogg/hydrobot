<?php

use Core\Request;
use Core\Response;
use Core\Template;
use Core\Event;

Event::add('web::auth', function(){
	
	$response = null;
	if(@Request::Request()['password'] != MASTER_PASSWORD)
		if(isset($_SERVER['X_REQUESTED_WITH'])){
			Response::JSON(['status' => 'failed', 'message' => 'AUTH_REQUIRED']);
		} else
			$response = Template::Get('web/auth');
	else {
		$_SESSION['auth'] = 1;
		Response::Redirect('admin');
	}
	return $response;
	
});