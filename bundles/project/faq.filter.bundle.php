<?php

namespace Bundle;

use Bundle\DBObject;

class FaqFilter extends DBObject implements DBSelectable {
	
	public function setLastRequestTime($timestamp){
		$this->setField('last_request', (int)$timestamp);
		$this->Save();
	}
	
	public function getLastRequestTime(){
		return $this->getField('last_request');
	}
	
	public function getUserId(){
		return $this->getField('user_id');
	}
	
	public function getId(){
		return $this->getField('id');
	}
	
	private static $fields = [
		
		'id' => TYPE_DB_NUMERIC | TYPE_DB_PRIMARY,
		'user_id' => TYPE_DB_NUMERIC,
		'last_request' => TYPE_DB_NUMERIC
		
	];
	
	public function __construct($obj = null)
	{
		parent::__construct(self::$fields, 'faq_filter', $obj);
	}
	
	public static function Select($rules, $count = null, $start = 0)
	{
		return parent::__select($rules, 'faq_filter', FaqFilter::class, $count, $start);
	}
	
}