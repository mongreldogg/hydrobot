<?php

namespace Bundle;

use Bundle\DBObject;

class FaqQnA extends DBObject implements DBSelectable, DBDeletable {
	
	public function getId(){
		return $this->getField('id');
	}
	
	public function getAnswer(){
		return $this->getField('faq_answer');
	}
	
	public function setAnswer($answer){
		$this->setField('faq_answer', $answer);
	}
	
	public function getQuestion(){
		return $this->getField('faq_question');
	}
	
	public function setQuestion($question){
		$this->setField('faq_question', $question);
	}
	
	public function getCategoryId(){
		return $this->getField('category_id');
	}
	
	public function setCategoryId($id){
		$this->setField('category_id', $id);
	}
	
	private static $fields = [
		
		'id' => TYPE_DB_NUMERIC | TYPE_DB_PRIMARY,
		'category_id' => TYPE_DB_NUMERIC,
		'faq_question' => TYPE_DB_TEXT,
		'faq_answer' => TYPE_DB_TEXT
		
	];
	
	public function __construct($obj = null)
	{
		parent::__construct(self::$fields, 'faq_qa', $obj);
	}
	
	public static function Select($rules, $count = null, $start = 0)
	{
		return parent::__select($rules, 'faq_qa', FaqQnA::class, $count, $start);
	}
	
	public static function Delete($rules){
		parent::__delete($rules, 'faq_qa');
	}
	
}