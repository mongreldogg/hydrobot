<?php

namespace Bundle;

use Bundle\DBObject;

class FaqContact extends DBObject implements DBSelectable{
	
	private static $fields = [
		'id' => TYPE_DB_NUMERIC | TYPE_DB_PRIMARY,
		'user_id' => TYPE_DB_NUMERIC
	];
	
	public function __construct($obj = null)
	{
		parent::__construct(self::$fields, 'faq_contacts', $obj);
	}
	
	public static function Select($rules, $count = null, $start = 0)
	{
		return parent::__select($rules, 'faq_contacts', FaqContact::class, $count, $start);
	}
	
	public static function IsContacted($userId){
		$contact = self::Select(['user_id' => (int)$userId], 1);
		if($contact instanceof FaqContact) return true;
		return false;
	}
	
	public static function SetContacted($userId){
		if(!FaqContact::IsContacted(Telegram::getUserId())){
			$contact = new FaqContact(['user_id' => $userId]);
			$contact->Save();
		}
	}
	
}