<?php

namespace Bundle;

use Bundle\DBObject;

class FaqCategory extends DBObject implements DBSelectable, DBDeletable {
	
	public function getId(){
		return $this->getField('id');
	}
	
	public function setCategoryName($catName){
		$this->setField('category', $catName);
	}
	
	public function getCategoryName(){
		return $this->getField('category');
	}
	
	private static $fields = [
		
		'id' => TYPE_DB_NUMERIC | TYPE_DB_PRIMARY,
		'category' => TYPE_DB_TEXT
		
	];
	
	public function __construct($obj = null){
		parent::__construct(self::$fields, 'faq_categories', $obj);
	}
	
	public static function Select($rules, $count = null, $start = 0)
	{
		return parent::__select($rules, 'faq_categories', FaqCategory::class, $count, $start);
	}
	
	public static function Delete($rules){
		parent::__delete($rules, 'faq_categories');
	}
	
}