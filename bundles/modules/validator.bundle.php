<?php

namespace Bundle;

class Validator {
	
	public static function Domain($domain) {
		return preg_match('/^([A-Za-z0-9]|-|.)+\.[A-Za-z0-9]+$/', $domain);
	}
	
}