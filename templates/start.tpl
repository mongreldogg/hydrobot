Hey! I am {project_name} bot!
Please pick up a question category by typing it's number from a list below.
You can also find a question of your interest typing some words separated with spaces and finishing your sentence with "?".

[category]{number}: {category}
[/category]