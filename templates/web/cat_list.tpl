<h2>Categories</h2>
<br/>
<div class="editor hidden">

	<span class="editor-close">Close</span>
	Category name:<br/>
	<input type="hidden" data-field="id" />
	<input type="text" data-field="category" /><br/>
	<input type="button" class="btn btn-edit" data-method="edit_category" value="Edit" />
	<input type="button" class="btn btn-delete" data-method="delete_category" value="Delete" />
	<input type="button" class="btn btn-create" data-method="add_category" value="Add" />

</div>
<div class="item_list">
	[cat]
	<div class="list-row" data-id="{id}">
		<div class="list-column hidden" data-field="id">{id}</div>
		<div class="list-column num">{counter}</div>
		<div class="list-column edit">Edit</div>
		<div class="list-column questions"><a href="admin?act=question_list&category={id}">Questions</a></div>
		<br />
		<div class="list-column category" data-field="category">{category}</div>
	</div>
	[/cat]
	<div class="list-template">
		<div class="list-column hidden" data-field="id"></div>
		<div class="list-column num"></div>
		<div class="list-column edit">Edit</div>
		<div class="list-column questions"><a href="admin?act=question_list&category=">Questions</a></div>
		<br />
		<div class="list-column category" data-field="category"></div>
	</div>
	<br/>
	<br/>
	<input type="button" class="btn-add-new" value="Add" />
</div>
