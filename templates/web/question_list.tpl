<h2>Questions: {catName}</h2>
<br/>
<div class="editor hidden">

	<span class="editor-close">X</span>
	<input type="hidden" data-field="id" />
	<input type="hidden" data-field="category_id" data-default="{categoryId}" />
	Question:<br/>
	<textarea data-field="faq_question" ></textarea><br/>
	Answer:<br/>
	<textarea data-field="faq_answer" ></textarea><br/>
	<input type="button" class="btn btn-edit" data-method="edit_question" value="Edit" />
	<input type="button" class="btn btn-delete" data-method="delete_question" value="Delete" />
	<input type="button" class="btn btn-create" data-method="add_question" value="Add" />

</div>
<div class="item_list">
	[question]
	<div class="list-row" data-id="{id}">
		<div class="list-column hidden" data-field="id">{id}</div>
		<div class="list-column num">{counter}</div>
		<div class="list-column edit">Edit</div>
		<div class="list-column hidden" data-field="category_id">{category_id}</div>
		<div class="list-column question">{short_q}</div>
		<div class="list-column hidden" data-field="faq_question">{faq_question}</div>
		<!--<div class="list-column answer">{short_a}</div>-->
		<div class="list-column hidden" data-field="faq_answer">{faq_answer}</div>
	</div>
	[/question]
	<div class="list-template">
		<div class="list-column hidden" data-field="id"></div>
		<div class="list-column num"></div>
		<div class="list-column edit">Edit</div>
		<div class="list-column hidden" data-field="category_id"></div>
		<div class="list-column question"></div>
		<div class="list-column hidden" data-field="faq_question"></div>
		<!--<div class="list-column answer"></div>-->
		<div class="list-column hidden" data-field="faq_answer"></div>
	</div>
	<br/>
	<br/>
	<input type="button" class="btn-add-new" value="Add" />
	<a href="admin" class="link_back">Back</a>
</div>