<!DOCTYPE html>
<html>
	<head>
		<title>{projectName} admin panel</title>
		<base href="https://{base}">
		<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
		<meta name="viewport" content="width=device-width">
		<link rel="stylesheet" type="text/css" href="files/css/style.css"/>
		<script type="text/javascript" src="files/js/jquery.js"></script>
		<script type="text/javascript" src="files/js/main.js"></script>
	</head>
<body>
	{content}
</body>
</html>