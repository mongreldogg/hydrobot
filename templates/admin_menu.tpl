Hello, administrator!
Here is the list of usable commands for you:

/select_category - Select a category from a list or create new
/delete_category - Delete a category selected from a list

/add_question - Add a new question (needs category selected)
/delete_question - Delete a question from a list of questions related to a cateegory selected

/quit - Quit admin menu and continue as a user.