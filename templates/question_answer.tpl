Question:
{question}

Answer:
{answer}

If this question is not what you were interested in, please don't hesitate to use manual categories and questions selection.

If you have any other question, please type /start again.
Thanks for being interested in HydroMiner!