<?php

//TODO: implement admin banwords restriction avoidance

use Bundle\Telegram;
use Bundle\TelegramResponse;

use Core\Template;

use Bundle\FaqCategory;
use Bundle\FaqQnA;
use Bundle\FaqContact;
use Bundle\FaqFilter;

function getCategoriesList(){
	
	$cats = FaqCategory::Select([]);
	$_cats = [];
	$idx = 1;
	if(is_array($cats))
		foreach($cats as $category){
			if($category instanceof FaqCategory) $_cats[$idx++] = $category;
		}
	return $_cats;
}

function getFaqList($category){
	
	$cats = getCategoriesList();
	if($cats[$category] instanceof FaqCategory){
		$questions = FaqQnA::Select([
			'category_id' => $cats[$category]->getId()
		]);
		$_questions = []; $idx = 1;
		if(is_array($questions))
			foreach($questions as $question){
				if($question instanceof FaqQnA) $_questions[$idx++] = $question;
			}
		return $_questions;
	} else return [];
	
}

function findBestMatch($keywords){
	
	$qna = FaqQnA::Select([]);
	global $FORBIDDEN;
	if(!is_array($FORBIDDEN)) initForbiddenKeywords();
	$score = [];
	foreach($qna as $item=>$q){
		if(!isset($score[$item])) $score[$item] = 0;
		if($q instanceof FaqQnA){
			$question = strtolower($q->getQuestion());
			foreach($keywords as $keyword){
				$keyword = strtolower($keyword);
				if(
					preg_match("/(^$keyword | $keyword | $keyword\?)/", $question)
					&& !isForbidden($keyword)
				) $score[$item] += 1;
			}
		}
	}
	$result = array_keys($score, max($score))[0];
	if($score[$result] >= KEYWORDS_MATCH_MIN_COUNT)
		return $qna[$result];
	else return null;
	
}

function initForbiddenKeywords() {

	global $FORBIDDEN;
	
	$words = Template::Get('misc/forbidden_keywords');
	$FORBIDDEN = explode("\n", $words);
	foreach($FORBIDDEN as $key=>$word){
		$FORBIDDEN[trim(strtolower($word))] = 1;
		unset($FORBIDDEN[$key]);
	}
	$FORBIDDEN[""] = 1;
	
}

function isForbidden($word){
	
	global $FORBIDDEN;
	if(isset($FORBIDDEN[$word])) return true;
	return false;
	
}

function isBanwordUsed($sentence) {

	global $BANWORDS;
	if(!is_array($BANWORDS)) initBanwords();
	$sentence = explode(' ', $sentence);
	$result = false;
	if(is_array($sentence))
		foreach($sentence as $word){
			$word = trim(strtolower($word));
			$sentence[$word] = 1;
			foreach($BANWORDS as $wordset){
				if(count($wordset) == 1){
					if($word == $wordset[0]) {
						$result = true;
						break 1;
					}
				} else {
					foreach($wordset as $banword){
						$result = true;
						if(!isset($sentence[$banword])) {
							$result = false;
							break 1;
						}
					}
				}
			}
		}
	return $result;

}

function initBanwords(){

	global $BANWORDS;
	$BANWORDS = [];
	$words = Template::Get('misc/banwords');
	$words = explode("\n", $words);
	foreach($words as $word){
		$word = trim(strtolower($word));
		$word = explode(' ', $word);
		$BANWORDS[] = $word;
	}
	if(isset($BANWORDS[""])) unset($BANWORDS[""]);
	
}

function questionFrequencyCheck($userID){

	$filter = FaqFilter::Select([
		'user_id' => $userID
	], 1);
	if(!($filter instanceof FaqFilter)) {
		$filter = new FaqFilter(['user_id' => $userID, 'last_request' => time()]);
		$filter->Save();
	}
	elseif($filter->getLastRequestTime() + FAQ_FILTER_RESPONSE_INTERVAL > time()) {
		exit;
	}
	else $filter->setLastRequestTime(time());

}

Telegram::on('/admin', function(){

	if(!Telegram::isPrivate()) exit;
	
	Telegram::WaitFor(SECURE_ROUTE.'admin::password');
	TelegramResponse::Text(Template::Get('admin_password'))->Send();

});

Telegram::on(SECURE_ROUTE.'admin::password', function(){
	
	$password = Telegram::getCommand();
	if($password != MASTER_PASSWORD)
		TelegramResponse::Text(Template::Get('admin_wrong_password'))->Send();
	else {
		Telegram::WaitFor(SECURE_ROUTE.'admin::menu');
		TelegramResponse::Text(Template::Get('admin_menu'))->Send();
	}
	
});

Telegram::on(SECURE_ROUTE.'admin::menu', function(){
	
	$command = Telegram::getCommand();
	$data = json_decode(Telegram::getHandleData(), true);
	
	switch($command){
		
		case '/select_category':
			
			$response = Template::Get('admin_pickcat_response');
			$cats = getCategoriesList();
			$catItem = Template::Tag('category', $response, '{cats}');
			$catsList = '';
			foreach($cats as $number=>$category){
				$catsList .= Template::Replace([
					'number' => $number,
					'category' => $category->getCategoryName()
				], $catItem);
			}
			$response = Template::Replace(['cats' => $catsList], $response);
			Telegram::WaitFor(SECURE_ROUTE.'category::pick');
			TelegramResponse::Text($response)->Send();
			
			break;
			
		case '/delete_category':
			
			$response = Template::Get('admin_delcat_response');
			$cats = getCategoriesList();
			$catItem = Template::Tag('category', $response, '{cats}');
			$catsList = '';
			foreach($cats as $number=>$category){
				$catsList .= Template::Replace([
					'number' => $number,
					'category' => $category->getCategoryName()
				], $catItem);
			}
			$response = Template::Replace(['cats' => $catsList], $response);
			Telegram::WaitFor(SECURE_ROUTE.'category::delete');
			TelegramResponse::Text($response)->Send();
			
			break;
			
		case '/add_question':
			
			Telegram::WaitFor(SECURE_ROUTE.'question::add', json_encode($data, true));
			TelegramResponse::Text(Template::Get('admin_addquestion_response'))->Send();
			
			break;
			
		case '/delete_question':
			
			@$categoryId = $data['category'];
			if(!$categoryId){
				Telegram::WaitFor(SECURE_ROUTE.'admin::menu', json_encode($data));
				TelegramResponse::Text(
					Template::Get('admin_category_select')."\r\n\r\n".
					Template::Get('admin_menu')
				)->Send();
				exit;
			} else {
				$response = Template::Get('admin_question_list');
				$questions = FaqQnA::Select(['category_id' => $categoryId]);
				if(!is_array($questions)){
					Telegram::WaitFor(SECURE_ROUTE.'admin::menu', json_encode($data));
					TelegramResponse::Text(
						Template::Get('admin_no_questions')."\r\n\r\n".
						Template::Get('admin_menu')
					)->Send();
				}
				$qItem = Template::Tag('question', $response, '{questions}');
				$qList = '';
				foreach($questions as $number=>$question){
					if($question instanceof FaqQnA)
					$qList .= Template::Replace([
						'number' => $number + 1,
						'question' => $question->getQuestion()
					], $qItem);
				}
				$response = Template::Replace(['questions' => $qList], $response);
				Telegram::WaitFor(SECURE_ROUTE.'question::delete', json_encode($data, true));
				TelegramResponse::Text($response)->Send();
			}
			
			break;
			
		case '/quit':
			TelegramResponse::Text(Template::Get('admin_bye'))->Send();
			break;
		default:
			Telegram::WaitFor(SECURE_ROUTE.'admin::menu', json_encode($data, true));
			TelegramResponse::Text(Template::Get('not_recognized'))->Send();
	}
	
});

Telegram::on(SECURE_ROUTE.'category::pick', function(){
	
	$command = Telegram::getCommand();
	
	switch(true){
		case $command == 'new':
			Telegram::WaitFor(SECURE_ROUTE.'category::add');
			TelegramResponse::Text(Template::Get('admin_addcat_response'))->Send();
			break;
		case is_numeric($command):
			@$categoryId = getCategoriesList()[$command]->getId();
			if($categoryId){
				Telegram::WaitFor(SECURE_ROUTE.'admin::menu', json_encode([
					'category' => $categoryId
				], true));
				TelegramResponse::Text(
					Template::Get('admin_category_selected')."\r\n\r\n".
					Template::Get('admin_menu')
				)->Send();
			} else {
				Telegram::WaitFor(SECURE_ROUTE.'category::pick');
				TelegramResponse::Text(Template::Get('no_category'))->Send();
			}
			break;
		default:
			Telegram::WaitFor(SECURE_ROUTE.'category::pick');
			TelegramResponse::Text(Template::Get('not_recognized'))->Send();
	}
	
});

Telegram::on(SECURE_ROUTE.'category::add', function(){
	
	$categoryName = Telegram::getCommand();
	
	$category = new FaqCategory(['category' => $categoryName]);
	$category->Save();
	
	$categoryId = $category->getId();
	
	Telegram::WaitFor(SECURE_ROUTE.'admin::menu', json_encode([
		'category' => $categoryId
	], true));
	
	TelegramResponse::Text(
		Template::Get('admin_category_added')."\r\n\r\n".
		Template::Get('admin_menu')
	)->Send();
	
});

Telegram::on(SECURE_ROUTE.'category::delete', function(){
	
	$categoryId = Telegram::getCommand();
	if(!is_numeric($categoryId)){
		Telegram::WaitFor(SECURE_ROUTE.'admin_menu');
		TelegramResponse::Text(
			Template::Get('no_category')."\r\n\r\n".
			Template::Get('admin_menu')
		)->Send();
	} else {
		$list = getCategoriesList();
		@$cat = $list[$categoryId];
		if($cat instanceof FaqCategory) {
			FaqQnA::Delete(['category_id' => $cat->getId()]);
			FaqCategory::Delete(['id' => $cat->getId()]);
			Telegram::WaitFor(SECURE_ROUTE.'admin::menu');
			TelegramResponse::Text(
				Template::Get('admin_category_deleted')."\r\n\r\n".
				Template::Get('admin_menu')
			)->Send();
		}
		else {
			Telegram::WaitFor(SECURE_ROUTE.'admin_menu');
			TelegramResponse::Text(
				Template::Get('no_category')."\r\n\r\n".
				Template::Get('admin_menu')
			)->Send();
		}
	}
	
});

Telegram::on(SECURE_ROUTE.'question::add', function(){

	$question = Telegram::getCommand();
	//$categoryId = Telegram::getHandleData();
	$data = json_decode(Telegram::getHandleData(), true);
	@$categoryId = $data['category'];
	if(!$categoryId){
		Telegram::WaitFor(SECURE_ROUTE.'admin::menu', json_encode($data));
		TelegramResponse::Text(
			Template::Get('admin_category_select')."\r\n\r\n".
			Template::Get('admin_menu')
		)->Send();
		exit;
	}
	Telegram::WaitFor(SECURE_ROUTE.'answer::add', json_encode(
		['question' => $question, 'category' => $categoryId],
		true));
	TelegramResponse::Text(Template::Get('admin_addanswer_response'))->Send();

});

Telegram::on(SECURE_ROUTE.'answer::add', function(){
	
	$data = json_decode(Telegram::getHandleData(), true);
	$answer = Telegram::getCommand();
	$question = $data['question'];
	$categoryId = $data['category'];
	
	$qna = new FaqQnA([
		'category_id' => $categoryId,
		'faq_question' => $question,
		'faq_answer' => $answer
	]);
	$qna->Save();
	
	Telegram::WaitFor(SECURE_ROUTE.'admin::menu', json_encode($data, true));
	TelegramResponse::Text(
		Template::Get('admin_added')."\r\n\r\n".
		Template::Get('admin_menu')
	)->Send();
	
});

Telegram::on(SECURE_ROUTE.'question::delete', function(){
	
	$question = Telegram::getCommand();
	$data = json_decode(Telegram::getHandleData(), true);
	$category = $data['category'];
	if(is_numeric($question)){
		$question -= 1;
		$qna = FaqQnA::Select(['category_id' => $category]);
		@$q = $qna[$question];
		if($q instanceof FaqQnA) {
			FaqQnA::Delete(['id' => $q->getId()]);
			Telegram::WaitFor(SECURE_ROUTE.'admin::menu', json_encode($data, true));
			TelegramResponse::Text(
				Template::Get('admin_question_deleted')."\r\n\r\n".
				Template::Get('admin_menu')
			)->Send();
		}
		else {
			Telegram::WaitFor(SECURE_ROUTE.'admin::menu', json_encode($data, true));
			TelegramResponse::Text(
				Template::Get('no_category')."\r\n\r\n".
				Template::Get('admin_menu')
			)->Send();
		}
	} else {
		Telegram::WaitFor(SECURE_ROUTE.'admin::menu', json_encode($data, true));
		TelegramResponse::Text(
			Template::Get('no_category')."\r\n\r\n".
			Template::Get('admin_menu')
		)->Send();
	}
	
});

Telegram::on('/start', function(){
	
	if(!Telegram::isPrivate()) exit;
	FaqContact::SetContacted(Telegram::getUserId());
	
	$cats = getCategoriesList();
	
	$response = Template::Get('start');
	$response = Template::Replace(['project_name' => PROJECT_NAME], $response);
	$catItem = Template::Tag('category', $response, '{cats}');
	$list = '';
	foreach($cats as $number=>$category){
		if($category instanceof FaqCategory)
			$list .= Template::Replace([
				'number' => $number,
				'category' => $category->getCategoryName()
			], $catItem);
	}
	$response = Template::Replace(['cats' => $list], $response);
	Telegram::WaitFor(SECURE_ROUTE.'category::select');
	TelegramResponse::Text($response)->Send();

});

Telegram::on(SECURE_ROUTE.'category::select', function(){
	
	$cats = getCategoriesList();
	$category = Telegram::getCommand();
	if(is_numeric($category)){
		@$categoryId = $cats[$category]->getId();
		if(!$categoryId){
			Telegram::WaitFor(SECURE_ROUTE.'category::select');
			TelegramResponse::Text(Template::Get('no_category'))->Send();
		} else {
			$questions = getFaqList($category);
			$response = Template::Get('questions');
			$questionItem = Template::Tag('question', $response, '{questions}');
			$list = '';
			foreach($questions as $number=>$question){
				if($question instanceof FaqQnA)
					$list .= Template::Replace([
						'number' => $number,
						'question' => $question->getQuestion()
					], $questionItem);
			}
			$response = Template::Replace([
				'questions' => $list
			], $response);
			Telegram::WaitFor(SECURE_ROUTE.'question::select', $category);
			TelegramResponse::Text($response)->Send();
		}
	} elseif (preg_match('/.*\?$/', $category)) {
		$category = str_replace('?', '', $category);
		$keywords = preg_split('/ /', $category);
		if(!is_array($keywords)) $keywords = [$keywords];
		$qna = findBestMatch($keywords);
		if($qna instanceof FaqQnA){
			$response = Template::Get('question_answer');
			$response = Template::Replace([
				'question' => $qna->getQuestion(),
				'answer' => $qna->getAnswer()
			], $response);
			TelegramResponse::Text($response)->Send();
		} else TelegramResponse::Text(Template::Get('no_match'))->Send();
	} else {
		Telegram::WaitFor(SECURE_ROUTE.'category::select');
		TelegramResponse::Text(Template::Get('no_category'))->Send();
	}
	
});

Telegram::on(SECURE_ROUTE.'question::select', function(){
	
	$question = Telegram::getCommand();
	$category = Telegram::getHandleData();
	if(is_numeric($question)){
		$qna = getFaqList($category);
		$response = Template::Get('answer');
		$response = Template::Replace(['answer' => $qna[$question]->getAnswer()], $response);
		TelegramResponse::Text($response)->Send();
	} else {
		Telegram::WaitFor(SECURE_ROUTE.'question::select', $category);
		TelegramResponse::Text(Template::Get('no_category'))->Send();
	}
	
});

Telegram::on('.*', function(){
	
	if(Telegram::isPrivate()) {
		TelegramResponse::Text(Template::Get('not_recognized'))->Send();
	} else {
		$command = Telegram::getCommand();
		if(isBanwordUsed($command)) Telegram::DeleteMessage();
		if (preg_match('/.*\?$/', $command)) {
			$category = str_replace('?', '', $command);
			$keywords = preg_split('/ /', $category);
			if (!is_array($keywords)) $keywords = [$keywords];
			$qna = findBestMatch($keywords);
			if ($qna instanceof FaqQnA) {
				questionFrequencyCheck(Telegram::getUserId());
				$response = null;
				switch(GROUP_RESPONSE_MODE){
					case 0:
						$response = Template::Get('question_answer');
						$response = Template::Replace([
							'question' => $qna->getQuestion(),
							'answer' => $qna->getAnswer()
						], $response);
						TelegramResponse::Text($response)->Send(Telegram::getUserId());
						break;
					case 1:
						$response = Template::Get('group_answer');
						$response = Template::Replace([
							'question' => $qna->getQuestion(),
							'answer' => $qna->getAnswer(),
							'username' => Telegram::getUserName()
						], $response);
						TelegramResponse::Text($response)->Send(Telegram::getChatId());
						break;
					case 2:
						if(FaqContact::IsContacted(Telegram::getUserId())){
							$response = Template::Get('question_answer');
							$response = Template::Replace([
								'question' => $qna->getQuestion(),
								'answer' => $qna->getAnswer()
							], $response);
							TelegramResponse::Text($response)->Send(Telegram::getUserId());
						} else {
							$response = Template::Get('advice_contact');
							$response = Template::Replace([
								'username' => Telegram::getUserName(),
								'link' => TELEGRAM_BOT_NAME
							], $response);
							TelegramResponse::Text($response)->Send(Telegram::getChatId());
						}
						break;
				}
			}
		}
	}

});