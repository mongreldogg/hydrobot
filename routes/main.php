<?php

//TODO: implement web UI banwords and restricted keywords management

use Core\Route;
use Core\Response;
use Core\Event;
use Core\Template;

Route::on('admin', function(){
	
	Template::SetGlobal('projectName', PROJECT_NAME);
	Template::SetGlobal('domain', HOSTNAME);
	Template::SetGlobal('base', HOSTNAME.ROOT_DIR);
	$response = null;
	if(!isset($_SESSION['auth'])) $response = Event::raise('web::auth');
	else $response = Event::raise('web::faq');
	Template::SetGlobal('content', Template::Render($response, []));
	Response::HTML(Template::Render(Template::Get('web/index'), []));
	
});