/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50718
Source Host           : 127.0.0.1:3306
Source Database       : hydrobot

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2018-03-17 09:39:47
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `bot_users`
-- ----------------------------
DROP TABLE IF EXISTS `bot_users`;
CREATE TABLE `bot_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wait_handle` text,
  `handle_data` text,
  `telegram_user_id` bigint(20) NOT NULL,
  `slack_user_id` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_BOT_USER_ID` (`telegram_user_id`,`slack_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bot_users
-- ----------------------------
INSERT INTO `bot_users` VALUES ('1', '', '', '123', '');
INSERT INTO `bot_users` VALUES ('2', '__category::select', '', '159285525', '');
INSERT INTO `bot_users` VALUES ('3', '', '', '488181656', '');
INSERT INTO `bot_users` VALUES ('4', '', '', '0', '');
INSERT INTO `bot_users` VALUES ('5', '', '', '404793311', '');
INSERT INTO `bot_users` VALUES ('6', '', '', '535772105', '');
INSERT INTO `bot_users` VALUES ('7', '', '', '404753171', '');
INSERT INTO `bot_users` VALUES ('8', '__category::select', '', '308783010', '');

-- ----------------------------
-- Table structure for `faq_categories`
-- ----------------------------
DROP TABLE IF EXISTS `faq_categories`;
CREATE TABLE `faq_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_FAQ_CATEGORY_NAME` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of faq_categories
-- ----------------------------
INSERT INTO `faq_categories` VALUES ('2', 'H2O Token');
INSERT INTO `faq_categories` VALUES ('4', 'H3O Token');
INSERT INTO `faq_categories` VALUES ('7', 'Hydrominer Affiliate Program');
INSERT INTO `faq_categories` VALUES ('1', 'HydroMiner General Questions');
INSERT INTO `faq_categories` VALUES ('6', 'HydroMiner Mining Portal');
INSERT INTO `faq_categories` VALUES ('8', 'KYC details');
INSERT INTO `faq_categories` VALUES ('22', 'New category');
INSERT INTO `faq_categories` VALUES ('10', 'Technical questions');

-- ----------------------------
-- Table structure for `faq_contacts`
-- ----------------------------
DROP TABLE IF EXISTS `faq_contacts`;
CREATE TABLE `faq_contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_CONTACT_USER_ID` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of faq_contacts
-- ----------------------------
INSERT INTO `faq_contacts` VALUES ('1', '123');

-- ----------------------------
-- Table structure for `faq_filter`
-- ----------------------------
DROP TABLE IF EXISTS `faq_filter`;
CREATE TABLE `faq_filter` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `last_request` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of faq_filter
-- ----------------------------
INSERT INTO `faq_filter` VALUES ('1', '123', '1521271781');

-- ----------------------------
-- Table structure for `faq_qa`
-- ----------------------------
DROP TABLE IF EXISTS `faq_qa`;
CREATE TABLE `faq_qa` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) NOT NULL,
  `faq_question` text NOT NULL,
  `faq_answer` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of faq_qa
-- ----------------------------
INSERT INTO `faq_qa` VALUES ('1', '1', 'What is HydroMiner?', 'HydroMiner is a cryptocurrency mining company, which uses green energy drawn from the hydro power station in the Alpine region of Europe. Hydro power is generally thought to be one of the most effective and lowest-cost renewable nergy resources.\r\n\r\nHydroMiner sold a voucher for mining time in their first ICO, the H2O token, which can be redemed in Februrary in the mining portal or later changed to the H3O token. Soon the presale of the H3O token will start which will be a compliant security token. The H3O token will grant the token holder participation in profits, voting rights and has many more features. One important characteristic is the alignment of interest between the shareholders and the token holders, all of them want to maximaze the profit of the Hydrominer company.');
INSERT INTO `faq_qa` VALUES ('3', '1', 'Where is Hydrominer currently listed / traded?', 'Etherdelta, coss, nebula and idex.market.');
INSERT INTO `faq_qa` VALUES ('4', '4', 'What are your facilities\' capacities and what are your plans for expansion?', 'Hydrominer rents the first hydro power station in Schönberg with an output of up to 290 kWh in Q2 2017. Around 120 mining units are deployed there,, each rig containing 6-10 GPU cards. The second Miningfarm near Waidhofen an der Ybbs was build in November. \n 250 Antminers S9 and a container with 1152 GPU cards are deployed there, with a constant energy supply of 600 kW. Currently we look at hydro power stations in Georgia and Canada as there are a lot of advantages for miners in these countries.');
INSERT INTO `faq_qa` VALUES ('5', '5', 'When did mining start?', 'All proceeds from the initial mining period, when it wasn\'t possible due to the mining portal development to redeem H2O tokens, will be used fpr the company\'s expansion. So H2O token holders will benefit 100% from this as the team didn\'t take a percentage from these mining profits. Mining portal will open in February 2018.');
INSERT INTO `faq_qa` VALUES ('6', '6', 'When will the mining calculator be released?', 'With the portal implementation');
INSERT INTO `faq_qa` VALUES ('7', '1', 'What kind of diversification strategies does HydroMiner have for the hardware?', 'Preorders are placed with the well known companies in the field, usually 2 or 3 months ahead of delivery, this is usually done with about 30% of the funds.\nSpot orders Spot order give us the most felxibility to react to changes in the market.  Also Hydrominer also seeks for new opportunities. In germany for example we have aan ASICS supplier under NDA (non-disclosure agreement) where we have already purchase planned in Q4 of 2018');
INSERT INTO `faq_qa` VALUES ('8', '5', 'Will there be open-ended contracts for sha-256 and the price per Th/s?', 'No.');
INSERT INTO `faq_qa` VALUES ('9', '1', 'Can you explain how the real estate costs of hydro plants are underwritten into dividend distribution of hydro?', 'We don\'t disclose the exact numbers, the price range for a kW is between 4 cents/kW and 6 cents/kW');
INSERT INTO `faq_qa` VALUES ('10', '1', 'What is the contract length of one token for mining and ghs for 1 token?', 'See mining portal. length is always 2 years.');
INSERT INTO `faq_qa` VALUES ('11', '1', 'In the literature the tokens are valued against kilowatt hours. So, why are the plans priced in gigahash instead of kilowatt hours?', 'Yes, you can see in the other screen shots that the figures are converted into kWh, at the point of sale, so you know how much you are getting in kWh.');
INSERT INTO `faq_qa` VALUES ('12', '4', 'With the change from h20 to h30, I understand that token holders would be receiving a securitized version of the token and those who convert would be receiving a bonus. For those of us who aren\'t well-versed in securities, what does this mean exactly? What\'s the difference between a normal token and a security?', 'A securtity token is new class of a token. The H3O token will be a participatory right to the proceeds of the company. The H3O token will have features like a tag-along-right, voting rights and will enable participation of profits for the token holders. So there is an aligned interest between the shareholders of the company and the H3O token holders, they all want to maximize the profits of the company. \nIf the company makes an IPO the participatory rights/ tokens will propaply converted into shares of the company at a discount to the IPO price.If the company does not make an IPO, but the founders sell the company (before an IPO) the H3O token holders will receive 80% of the selling proceeds.');
INSERT INTO `faq_qa` VALUES ('13', '4', 'I remember the old calculator.  The ROI was amazing!  Why was it taken down? Was it because we made an amazing investment and you guys felt you got the short end for all your hard work?  If so, you have to deal with it.  You can\'t just change the deal afterwards to suit yourself. A deals a deal isn\'t it?', 'https://medium.com/@nadine_3242/return-of-investment-6cb95aae4195');
INSERT INTO `faq_qa` VALUES ('15', '4', 'Do we know how often the mining contracts will have a pay out? Was it only after the 24month contract finished?', 'You can choose how often you want to be paid out as long as you reach the payout minimum.');
INSERT INTO `faq_qa` VALUES ('16', '4', 'What is the trade volume of Hydrominer right now?', 'Volume for 08.02.18 (GMT-5) has reached roughly $9,500! \n\n6.033 ETH on EtherDelta + 5.686 ETH on Coss, totalling 11.719ETH. \n\nSo taking the current exchange rate of $818.29, we’ve passed the $9,500 mark. \n\nThis isn’t taking into account the exact ETH price at the times of these transactions of course, but with approximately $1,000 more we’ll definitely reach that elusive $10k mark.');
INSERT INTO `faq_qa` VALUES ('17', '6', 'So if we invested into the presale; am i remembering correctly that each year we get our bonus amount of H20 TOKENS, every year forever?', 'You received a 20 percent bonus for your investment. H20 will only become available at the time when HydroMiner has the capacity to expand the inftrastructure needed to support additional mining contracts. It is possible that different structures for bonuses will become available at various points of issuance of H2O, depending on advances in the technology and resources that are available to HydroMiner.\n\nThe ICO bonus is only applicable if you want to purchase more mining contracts after your first 2 year contract, up to the same amount you invested in the ICO.\nYou won\'t receive h2o every year.');
INSERT INTO `faq_qa` VALUES ('18', '6', 'Is there a minimum number of h20 token to start a mining contract? In other words, can 1 h20 token start a 2 year contract of 12.5 kWh?', 'There is no minimum redeem amount for H2O, but there is a minimum payout for the currencies, at the moment they are as follows: ETH: 0,01, BTC: 0,02, BCH: 0,001, ETC: 1, ZEC: 0,1');
INSERT INTO `faq_qa` VALUES ('19', '4', 'I am currently in, or am an US citzen abroad.  Can I convert my H2O into H3O?', '(3) In particular, citizens of the United States of America (US) or US-residents, and ‘U.S. persons’ (“US Persons”) under Regulation S of the US Securities Act of 1933, as well as Australian, Canadian, British, Japanese nationals and persons resident in such countries, are prohibited from accessing the Website and the Information. This also applies to the distribution, publication or dissemination of the Information in the USA, Australia, Canada, Great Britain or Japan or in other states and to their nationals, in which this is prohibited. (4) The Securities have not been and will not be registered under the US Securities Act of 1933. They were not approved for trade for the purposes of the US Commodities Exchange Act of 1936. The Securities may not be offered, sold or transferred within the United States or for or on behalf of US Persons.');
INSERT INTO `faq_qa` VALUES ('20', '4', 'Will there be annual or quarterly Shareholder letters? If so, what information regarding coins mined, coins held, coins sold will be shared?', '(5) Every interested investor is obliged to independently verify whether he or she is authorized to do so before accessing this Website or accessing the Information contained therein. The Issuer is neither responsible nor liable for the dissemination of the Website or the Information to persons who have given false information about their right to access the Website or the Information. The Issuer rejects any liability for any claims or damages that may result from an unauthorized access or reading of the Website or the Information contained therein.');
INSERT INTO `faq_qa` VALUES ('21', '4', 'Do I need to go through KYC process if I decide to keep my H2O tokens for exchange with h3O later?', 'Yes there will be a form of kyc acquiring h3o but it is not defined right now. Right now you do not need to upload any documents for the kyc process as it is only for mining');
INSERT INTO `faq_qa` VALUES ('24', '4', 'WHEN MOON?', '¯\\_(ツ)_/¯');
INSERT INTO `faq_qa` VALUES ('25', '7', 'What is the HydroMiner Affiliate program?', 'Our program is free to join, it\'s easy to sign-up and requires no technical knowledge. Affiliate programs are common throughout the Internet and offer website owners an additional way to profit from their websites. Affiliates generate traffic and sales for commercial websites and in return receive a commission payment. More Info at: https://www.hydrominer.org/affiliate-program/');
INSERT INTO `faq_qa` VALUES ('26', '7', 'How does the HydroMiner Affiliate program work?', 'When you join our affiliate program, you will be supplied with a range of banners and textual links that you place within your site. When a user clicks on one of your links, they will be brought to our website and their activity will be tracked by our affiliate software. You will earn a commission based on your commission type. More Info at https://www.hydrominer.org/affiliate-program/');
INSERT INTO `faq_qa` VALUES ('27', '7', 'How much can i earn with the HydroMiner Affiliate program?', 'You can earn 10% for each sale you deliver. You can also build your own sub-affilates where you can earn additional 5% on their sales commission. More Info at: https://www.hydrominer.org/affiliate-program/');
INSERT INTO `faq_qa` VALUES ('28', '8', 'WHEN MOON?', '¯\\_(ツ)_/¯');
INSERT INTO `faq_qa` VALUES ('31', '10', 'whenmoon?', 'whoknows');
INSERT INTO `faq_qa` VALUES ('32', '0', 'asdf', '');
INSERT INTO `faq_qa` VALUES ('33', '0', '123123', 'fdsa');
INSERT INTO `faq_qa` VALUES ('37', '0', 'WHEN MOON?', 'I DONT KNOW');
INSERT INTO `faq_qa` VALUES ('38', '0', 'WHEN MOON?', 'IDK');
INSERT INTO `faq_qa` VALUES ('39', '0', 'WHEN MOON?', 'IDK');
INSERT INTO `faq_qa` VALUES ('42', '2', 'WHEN MOON?', 'IDK');
