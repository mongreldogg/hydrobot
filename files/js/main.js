
$(document).ready(function(){

    $('.editor').removeClass('hidden');
    $('.editor').fadeOut(0);

    $('.editor-close').on('click', function(){
        $('.editor').fadeOut(200);
    });

    $(document).on('click', '.list-column.edit', function(){
        $('.btn-create').fadeOut(0);
        $('.btn-edit').fadeIn(0);
        $('.btn-delete').fadeIn(0);
        var parent = $(this).parent('*[data-id]');
        $.each(parent.find('[data-field]'), function(){
            var field = $(this).attr('data-field');
            $('.editor *[data-field="'+field+'"]').val($(this).html());
        });
        $('.editor').fadeIn(200);
    });

    $('.editor .btn').on('click', function(){

        var formFields = {},
            form = $(this).parent('.editor'),
            button = $(this),
            method = $(this).attr('data-method');
        $.each(form.find('[data-field]'), function(){
            formFields[$(this).attr('data-field')] = $(this).val();
        });
        $.ajax({
            method: 'POST',
            url: 'admin?act='+method,
            data: formFields
        }).done(function(response){
            var item;
            switch(true){
                case button.hasClass('btn-delete'):
                    $('.list-row[data-id="'+response.delete_id+'"]').remove();
                    break;
                case button.hasClass('btn-edit'):
                    item = $('.list-row[data-id="'+response.item.id+'"]');
                    $.each(response.item, function(idx, val){
                        item.find('[data-field="'+idx+'"]').html(val);
                    });
                    if(typeof(editCallbacks[method]) == 'function') editCallbacks[method](response, item);
                    break;
                case button.hasClass('btn-create'):
                    item = $('.item_list').find('.list-template')[0].outerHTML;
                    item = $(item);
                    item.removeClass('list-template');
                    item.addClass('list-row');
                    item.attr('data-id', response.item.id);
                    $.each(response.item, function(idx, val){
                        item.find('[data-field="'+idx+'"]').html(val);
                    });
                    if(typeof(editCallbacks[method]) == 'function') editCallbacks[method](response, item);
                    item.insertBefore($('.list-template'));
            }
            var cntr = 1;
            $.each($('.list-row'), function(){
                $(this).find('.num').html(cntr++);
            });
        });
        $('.editor').fadeOut(200);
    });

    $('.btn-add-new').on('click', function(){
        $('.editor [data-field]').val('');
        $.each($('.editor [data-default]'), function(){
            $(this).val($(this).attr('data-default'));
        });
        $('.btn-edit').fadeOut(0);
        $('.btn-delete').fadeOut(0);
        $('.btn-create').fadeIn(0);
        $('.editor').fadeIn(200);
    });

});

var editCallbacks = {

    add_category: function(response, item){
        item.find('.questions a').attr('href', 'admin?act=question_list&category='+response.item.id);
    },
    edit_category: function(response, item){

    },
    add_question: function(response, item){
        var question = response.item.faq_question,
            answer = response.item.faq_answer;
        if(question.length > 48) question = question.substr(0, 48)+'...';
        if(answer.length > 48) answer = answer.substr(0, 48)+'...';
        item.find('.question').html(question);
        item.find('.answer').html(answer);
    },
    edit_question: function(response, item){
        var question = response.item.faq_question,
            answer = response.item.faq_answer;
        if(question.length > 48) question = question.substr(0, 48)+'...';
        if(answer.length > 48) answer = answer.substr(0, 48)+'...';
        item.find('.question').html(question);
        item.find('.answer').html(answer);
    }

};