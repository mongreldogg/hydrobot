Telegram Bot: FAQ

-------------
Вступление
-------------

Бот предназначен для обработки вопросов пользователей и возврата доступных ответов из FAQ списка.

-------------
Особенности
-------------

1. Вопросы разделены по категориям
2. Ответы на вопросы могут быть получены по совпадению ключевых слов в вопросе пользователя.
3. Взаимодействие с группой Telegram: бот отвечает на вопросы пользователей в группе.
4. Несколько способов модерации категорий, вопросов и ответов: чат с ботов и веб-интерфейс.

-------------
Примеры использования
-------------

Пользователь может обмениваться с ботом личными сообщениями, чтобы получить ответы на вопросы.
Чтобы использовать бота, пользователь может ввести команду /start и получить список категорий вопросов, интересующих его.
Так же, при получении списка категорий пользователь может ввести свой вопрос, заканчивая предложение знаком вопроса.
Таким образом, бот разделит предложение на кючевые слова и самостоятельно найдёт вопрос по наибольшему совпадению ключевых слов
и даст ответ на него, так что пользователю не обязательно искать вопрос среди категорий.

Бот так же отвечает на вопросы и в группе - пользователь может задать его и бот ответит наиболее подходящей парой вопрос-ответ.

Примеры использования пользователем (П - сообщение пользователя, Б - ответ бота)

Пример №1:

П: /start
Б: Приветствие, список категорий
П: {номер категории}
Б: Список вопросов
П: номер вопроса
Б: Ответ

Пример №2:
П: /start
Б: Приветствие, список категорий
П: вопрос, быть или не быть?
Б: Вопрос, совпадающий по наибольшему количеству слов в вопросе пользователя, и ответ на него

-------------
FAQ модерация
-------------

Существует два способа модерации категорий, вопросов и ответов:

1. Модерация через чат.

Пользователь, знающий пароль для доступа к панели модерации, может написать боту следующие сообщения:

П: /admin
Б: Запрос пароля
П: <password>
Б: Приветствие, список доступных команд

Где <password> - пароль, установленный в конфигурации бота.

П: /select_category
Б: Список категорий
П: (Номер категории)
Б: Категория выбрана, снова - список доступных команд.

П: /add_question
Б: Введите вопрос для добавления
П: (вопрос)
Б: Введите ответ на вопрос
П: (ответ)
Б: Вопрос и ответ добавлены

П: /delete_question
Б: Выберите вопрос для удаления (список)
П: (номер вопроса из списка)
Б: Вопрос удален, снова - список команд

П: /delete_category
Б: Выберите категорию для удаления (список)
П: (номер категории)
Б: Категория удалена, снова - список команд (для дальнейшей работы над вопросами нужно снова выбрать категорию - /select_category)

Когда пользователь получает доступ к чат-панели, он может добавлять и удалять категории и пары вопрос-ответ.

2. Модерация через веб-интерфейс.

Это более удобный способ модерации категорий, вопросов и ответов. Позволяет не только добавлять и удалять вопросы и категории,
но и изменять их, без необходимости удаления и добавления заново при необходимости в исправлении.

Чтобы получить доступ к веб-панели модерации, пользователю нужно перейти по ссылке https://<domain>/<project_name>/admin
и ввести тот же пароль, который используется и для входа в панель через чат.

При посещении страницы веб-модерации и вводе пароля, пользователю предоставляется список уже существующих категорий и кнопка Add.
При нажатии кнопки Edit для категории, открывается окно, позволяющее изменить название категории или удалить её.
При нажатии кнопки Add на странице списка категорий, открывается окно, в котором можно задать имя новой категории для создания.
При нажатии кнопки Questions для категории, открывается список вопросов, относящихся к выбранной категории.
При нажатии на Edit для вопроса, открывается окно редактирования вопроса и ответа, с возможностью изменить или удалить пару вопрос-ответ.
При нажатии на Add на странице вопросов, открывается окно, в котором пользователь может ввести вопрос и ответ для добавления.

-------------
Установка
-------------

В данный момент бот в большинстве случаев протестирован и лучше всего работает при соблюдении следующих требований:
- Используется VPS с предпочтительно установленным Debian 9
- Веб-сервер: nginx
- PHP версии 7.0
  - Модуль PHP FPM установлен для Nginx
  - PHP MySQL расширение установлено
  - PHP CURL расширение установлено
- База данных: MySQL (MariaDB)
- Разрешены входящие подключения на порт 443 для HTTPS запросов
- SSL сертификат от LetsEncrypt установлен
- Файлы проекта (предпочтительно использовать Git для последующих обновлений)
- Используется клиент Telegram для создания и управления ботом


*Установка: программное обеспечение.*

Следующие Debian команды должны быть запущены:

apt-get update && apt-get upgrade
apt-get install nginx php7.0 php7.0-fpm php7.0-curl php7.0-mysql mysql-server git python-certbot-nginx

*Установка: конфигурация Nginx*

Прежде всего, для развертывания бота необходим домен, ассоциированный с адресом VPS.
После того, как домен привязан к серверу, следует изменить следующую конфигурацию:

nano /etc/nginx/sites-enabled/default

- "server_name" должен быть изменен на домен, используемый для бота
- "index" строка должна заканчиваться с index.php
- "location" параметры должны быть дополнены следующей конфигурацией:

location /{project_name}/ {
	try_files $uri $uri/ /{project_name}/index.php?$args;
}

location ~ \.php$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
}

*Установка: файлы проекта*

Используется Git клиент для развертывания проекта на сервере:

cd /var/www/html
git clone https://{project files URL} {project_name}

Где:

- {project files URL} - путь к репозиторию, содержащему файлы проекта, к которому есть доступ
- {project_name} - наименвение папки проекта, указанное прежде в конфигурации Nginx

*Установка: база данных*

Для инициализации собственной базы данных бота, следует выполнить команды:

mysql
create database {database_name};
create user '{user}'@'%' identified by '{password}';
grant all privileges on {database_name}.* to '{user}'@'%' with grant option;
quit

Где:

- {database_name} - имя базы данных для проекта
- {user} - имя пользователя базы данных
- {password} - пароль пользователя базы данных

После создания базы данных и пользователя для неё, следует ввести следующие команды:

cd /var/www/html/{project_name}
mysql {database_name} < files/faqbot.sql

- {project_name} - наименование папки проекта
- {database_name} - наименование созданной базы данных

*Установка: конфигурация проекта*

cd /var/www/html/{project_name}/config
ls

Здесь размещены файлы конфигурации проекта.

Следует изменить конфигурацию в файле core/config.php в первую очередь:

nano core/config.php

Изменить наименование папки проекта (обязательно):

define('ROOT_DIR', '/{project_name}/');

Изменить домен, используемый в проекте (обязательно):

define('HOSTNAME', '{domain}');

Изменить название проекта (используется в шаблонах ответов бота и веб-интерфейса)

define('PROJECT_NAME', 'My Project FAQ');

Следующее - core/database.config.php:

nano core/database.config.php

Конфигурация:

define('DB_USER', '{user}');
define('DB_PASSWORD', '{password}');
define('DB_NAME', '{database_name}');

Изменить {user} на имя пользователя базы данных, {password} на пароль пользователя БД
и {database_name} на название созданной базы данных соответственно.
Все эти записи в конфигурации необходимы.

Чтобы изменить пароль от панели модерации, следует открыть faqbot.config.php:

nano faqbot.config.php

И изменить следующее:

define('MASTER_PASSWORD', '{your_mod_password}');

*Установка: бот Telegram*

- Обратиться https://t.me/BotFather
- Написать /newbot
- Бот попросит ввести имя для нового бота, может быть любым.
- После, бот попросит ввести ссылку на нового бота. Всегда должно заканчиваться на "bot"
- После, будет возвращен токен бота, который будет использоваться для операций над ним.
Следует хранить токен в секрете из соображений безопасности.

Так же, стоит ввести команду /mybots и выбрать бота для последующей конфигурации:

Bot Settings > Allow Groups > Установить в ON
Bot Settings > Groups Privace > Установить в OFF

После этого, открыть папку конфигурации снова и открыть файл telegram.config.php:

cd /var/www/html/{project_name}/config
nano telegram.config.php

Следующую конфигурацию необходимо изменить:

define('TELEGRAM_BOT_NAME', 'hydro_faq_bot');

Нужно изменить на ссылку, введенную при создании бота.

define('TELEGRAM_BOT_TOKEN', '564829450:AAFWZg3pbGW5cMe16wCHu6pqeOoAgIOwesc');

Следует изменить на токен, полученный у BotFather после создания бота.

*Установка: SSL сертификат*

Сертификат необходим для получения запросов от Telegram для обработки ботом.
Чтобы установить его, нужно ввести следующую команду:

certbot --authenticator standalone --installer nginx  -d {domain} --pre-hook "service nginx stop" --post-hook "service nginx start"

Где {domain} - как раз то, о чем мы думаем.

Будет запрошено принятие условий лицензии и контактный адрес для домена (может быть несуществующим)
и опция перенаправления всех запросов на HTTPS, которую необходимо включить, выбрав Yes.

*Установка: запуск бота*

Для того, чтобы дать Telegram понять, что бот готов обрабатывать запросы, нужно установить путь WebHook.
Делается это следующим образом (в консоли VPS):

curl -F "url=https://{domain}/{project_name}/bot" https://api.telegram.org/bot{token}/setWebHook

Где {domain} и {project_name} описаны ранее, {token} - токен Telegram бота, полученный при создании.

Так же, чтобы бот имел возможность обрабатывать вопросы пользователей в группах, ему необходимы права администратора