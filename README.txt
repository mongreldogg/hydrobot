Telegram Bot: FAQ

-------------
Introduction
-------------

A bot is designed to process user's questions and respond with available answers according to FAQ section of a project.

-------------
Features
-------------

1. Questions separated by FAQ sections
2. Questions available to answer by keywords match when a user asks about anything already described in a FAQ listing.
3. Telegram groups interaction: a bot answers questions asked in a group.
4. Multiple ways of FAQ sections and Q&A management: both in a direct chat and via web UI.

-------------
Example of use
-------------

A user can contact a bot directly to get answers.
To use a bot, a user can type /start and get a list of sections to find questions he is actually interested in.
Also, when a user gets a list of sections, he can ask a bot anything directly by ending his sentence with "?".
This way a bot will separate a sentence by words and find a best match in a whole list of questions,
so a user can get an answer when he's not sure which section he has to chose.

A bot also answers in a group without picking a section - a user can just write a question and he will receive best matching answer avaialble.


Example of bot usage (U - user's message, B - bot's response)

Example #1:

U: /start
B: Greeting, categories list
U: (category number)
B: Questions list
U: (question number)
B: Response

Example #2:
U: /start
B: Greeting, categories list
U: hey, i have a question, to be or not to be?
B: A question that mostly matches words of a question and a response if available

-------------
FAQ management
-------------

There are two ways of managing FAQ sections and questions.

1. Moderation through direct chat.

A user who knows a password to access a moderation panel can write the following in a direct messaging with a bot:

U: /admin
B: Please type password
U: <password>
B: Greeting, available commands list

Where <password> is a password defined for an exact project.

U: /select_category
B: Categories list
U: (category number)
B: Category selected! (commands list, again)

U: /add_question
B: Type a question to add
U: (question)
B: Type an answer to a question
U: (answer)
B: Q&A added

U: /delete_question
B: Pick a question to delete (list)
U: (question number)
B: Q&A deleted! (commands list, again)

U: /delete_category
B: Pick a category to delete (list)
U: (category number)
B: Category deleted! (commands list, again; need to select a category again to edit questions)


When a user enters an admin panel, he is able to add and delete sections and Q&A to a selected section.

2. Moderation through web UI.

This is more comfortable, but still optional way to moderate sections, questions and answers
with an ability to edit existing section names, FAQ questions and answer, so it's not required to delete and create them again
at the moment there are only changes required.

To access a web moderation UI, a user should enter https://<domain>/<project_name>/admin web page and type the same password
that is used in a direct chat management too.

When entering a web page and typing a password, a user will see an existing list of categories and Add button.
When clicked on Edit button for a category, a user will see a window where he can edit i'ts name or delete it.
When clicked on Add button at Categories list page, a user will see a window where he can type a name of a new category to add.
When clicked on Questions button for a category, a user will see a list of questions from exact category.
When clicked on Edit for a question, a user will see a window which allows him to edit question and answer or delete a pair.
When clicked on Add button on a questions page, a user will see an empty window to type question and answer to add.

-------------
Installation
-------------

Currently a bot is mostly tested and best performed passing the following requirements:
- A VPS with preferably Debian 9 installed
- Web server: Nginx
- PHP: version 7.0
  - PHP FPM module for Nginx
  - PHP MySQL extension
  - PHP CURL extension
- Database: MySQL (MariaDB)
- Incoming connections available on port 443 for HTTPS
- LetsEncrypt SSL certificate installed
- Project files package (preferable using Git for further updates)
- Telegram client to create and manage a bot

*Installation: VPS software.*

The following Debian commands should be executed:

apt-get update && apt-get upgrade
apt-get install nginx php7.0 php7.0-fpm php7.0-curl php7.0-mysql mysql-server git python-certbot-nginx

*Installation: Nginx configuration*

First of, there is a domain should be associated with a VPS address.
After a domain has been set to access a machine, there myst be a nginx configuration changed:

nano /etc/nginx/sites-enabled/default

- "server_name" entry must be changed to an exact domain used for a project
- "index" entry must content "index.php" at the end of a string
- "location" entries must be extended the following way:

location /{project_name}/ {
	try_files $uri $uri/ /{project_name}/index.php?$args;
}

location ~ \.php$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
}

*Installation: project files*

Use a Git client to deploy a project on your machine:

cd /var/www/html
git clone https://{project files URL} {project_name}

Where:

- {project files URL} is a repository URL which you have an access to
- {project_name} is the name of a folder which is previously defined in a Nginx configuration

*Installation: Database*

To intialize a bot's own database, the following commands must be executed:

mysql
create database {database_name};
create user '{user}'@'%' identified by '{password}';
grant all privileges on {database_name}.* to '{user}'@'%' with grant option;
quit

Where:

- {database_name} is an optional name of a database for a project
- {user} is a database user name that will have full access to a bot database
- {password} is a password for a database user

After initial database and user creation, the following commands will deploy a full database with required layout:

cd /var/www/html/{project_name}
mysql {database_name} < files/faqbot.sql

- {project_name} is a folder where project files located
- {database_name} is a name of a created database

*Installation: project configuration*

cd /var/www/html/{project_name}/config
ls

Here you will find a list of configuration files for a project.

You need to edit core/config.php first:

nano core/config.php

Change this one to set a default project folder (required):

define('ROOT_DIR', '/{project_name}/');

Change this one to set your domain name you got for a project:

define('HOSTNAME', '{domain}');

Change this one to define your project name (used in most significant templates of responses and Web UI):

define('PROJECT_NAME', 'My Project FAQ');

Second, core/database.config.php:

nano core/database.config.php

Configuration:

define('DB_USER', '{user}');
define('DB_PASSWORD', '{password}');
define('DB_NAME', '{database_name}');

Change {user} to database user name, {password} to database user password
and {database_name} to a created database name accordingly. All those configurations are required.

To change a moderation password, you can open:

nano faqbot.config.php

And change the following entry:

define('MASTER_PASSWORD', '{your_mod_password}');

*Installation: Telegram Bot*

- Contact https://t.me/BotFather
- Type /newbot
- It will ask you for a bot name to display for Telegram users, you can type any
- Then, it will ask you for a bot link to access it, it should always end by "bot"
- Then, it will send you a bot token you will need to use for bot operations.
Never share this token to anyone for security resons.

Also, type /mybots and pick your bot to change the following configuration:

Bot Settings > Allow Groups > Set to ON
Bot Settings > Groups Privace > Set to OFF

After all of that done, open config folder of a project again and open telegram.config.php:

cd /var/www/html/{project_name/config
nano telegram.config.php

The following entries are required to change:

define('TELEGRAM_BOT_NAME', 'hydro_faq_bot');

Must be changed to a bot link you have defined when created a bot.

define('TELEGRAM_BOT_TOKEN', '564829450:AAFWZg3pbGW5cMe16wCHu6pqeOoAgIOwesc');

Must be changed to a token you received from BotFather after you created a bot.

*Installation: SSL Certificate*

It's required to have an SSL certificate for all Telegram requests coming to a bot.
To install it, simply use the following command:

certbot --authenticator standalone --installer nginx  -d {domain} --pre-hook "service nginx stop" --post-hook "service nginx start"

Where {domain} is an exact thing you thought about.
It will ask you for a license agreement appliance, contact e-mail for your domain (can be non existing aswell)
and an option to redirect all requests to HTTPS or not, so you have to pick Yes.

*Installation: Running a bot*

To let Telegram know your bot is ready to process requests, you haveto set WebHook path.
Simply do the following:

curl -F "url=https://{domain}/{project_name}/bot" https://api.telegram.org/bot{token}/setWebHook

Where {domain} and {project_name} are described previously, and {token} is a Telegram bot token you have rceived after you created it.

Also, for a bot to access users questions inside a group, it has to become an administrator of a group straight after it is added.