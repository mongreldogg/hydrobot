<?php

//Use for URL and route definition itself
define('NOTFOUND', '404');
//Main URL directory assignment
define('ROOT_DIR', '/hydrobot/');

define('PACK_HTML', true);

//Disables caching when enabled
define('DEBUG_MODE', 0);
//Route prefix for secure routes for inner calls
define('SECURE_ROUTE', '__');

define('HOSTNAME', 'baubloxx.network');
define('FOREHOOD_DEAFULT_TIMEOUT', 1000); //ms

define('PROJECT_NAME', 'HydroMiner FAQ');

