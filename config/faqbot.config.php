<?php


define('MASTER_PASSWORD', 'aquaman42');

/*
 * Possible configurations:
 *
 * 0: A bot will never respond anything in a group
 * Instead, an advice to contact a bot to be able to receive responses should be put by a service manually
 *
 * 1: A bot will respond on all questions in a group
 * It will never require a user to contact a bot to receive their answers, but also it will fill a group with
 * bot answer every tme there is a question with keywords match written in a group chat
 *
 * 2: A bot will respond with an advisory to contact him to get answers directly
 * It will automatically take users into an action and have less spam in a group chat
 */
define('GROUP_RESPONSE_MODE', 1);
